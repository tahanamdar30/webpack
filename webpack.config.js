 const HtmlWebpackPlugin = require('html-webpack-plugin');

 // const ExtractTextPlugin = require("extract-text-webpack-plugin");



 const path = require('path')

 /*
 [hash]
 [chunkhash]
 [name]
 [id]
 [query]
 [contenthash]
 */



 module.exports = {


     mode: 'development',
     entry: {
         myfile: './src/engine.js',
         one: './one.js',
         two: './two.js',

     },



     output: {
         path: path.resolve(__dirname, 'dist'),
         filename: '[name]-[contenthash].bundle.js',
         clean: true,
         // publicPath: '/assets/',
         // libraryTarget: 'amd',
         // library: 'myfirstlibrary'
         // filename: 'bundle.js',
         // filename: '[name].bundle.js',

     },
     module: {
         rules: [

             {
                 test: /\.css$/,
                 use: ["style-loader", "css-loader"],
             },


             {


                 test: /\.(png|svg|jpg|jpeg|gif)$/i,
                 type: 'asset/resource',




             },



             // {
             //     test: /\.(eot|svg|ttf|woff|woff2)$/,
             //     loader: 'file-loader?name=/fonts/[name].[ext]'
             // },



         ],
     },



     devServer: {
         port: 8000,
         contentBase: path.join(__dirname, 'dist'),
         writeToDisk: false,

     },

     plugins: [
         new HtmlWebpackPlugin(

             {
                 filename: 'index.html',
                 title: 'webpack',
                 template: './my-index.html',
                 chunks: ['myfile']

             },

         ),


         new HtmlWebpackPlugin({
             filename: 'one.html',
             title: 'one',
             template: './one.html',
             chunks: ['one']


         }),
         new HtmlWebpackPlugin({
             filename: 'two.html',
             title: 'two',
             template: './two.html',
             chunks: ['two']


         })




     ],



 }